#WOL GUI

****

**Con Wol Gui podrás encender computadoras de manera remota haciendo uso del protocola Wake On Lan**
Para correr Wol Gui necesitas las siguientes dependencias y programas en tu computadora.

#####Requerimientos:
- Python 2.7
- Easygui
- Wakeonlan
- XTerm

#####Instalación

- **Instalación de las dependencias**
	- Para instalar python si estas en alguna distribución que deriva de **Debian** ejecuta lo siguiente:
	```bash
    $ sudo apt-get install python2.7
    ```
    - Para instalar Easygui debes tener instalado pip el gestor de paquetes de python vamos a instalar pip y easygui de la siguiente manera:
    ```bash
    $ sudo apt-get install python-pip
    $ sudo pip install easygui
    ```
    - Para instalar xterm y wakeonlan simplemente ejecutamos lo siguiente:
    ```bash
    $ sudo apt-get install wakeonlan xterm
    ```
- **Ejecución del programa**
	- Para ejecutar **WOL GUI** simplemente descargate el repositorio:
	```bash
    $ git clone https://gitlab.com/carbans/wol-gui.git
    $ cd wol-gui
    ```
    - Si queremos ejecutarlo desde el entorno gráfico simplemente haremos doble click sobre el fichero llamado **lanzar.sh**
    - Si queremos lanzarlo desde la consola ejecutaremos lo siguiente
    ```bash
    $ python run.py
    ```


**With Wol Gui you can turn on computer remotely using Wake On Lan protocol**
To run Wol Gui you need the following requeriments and programs on your computer.

#####Requeriments:
- Python 2.7
- Easygui
- Wakeonlan
- XTerm

#####Installation

- **Installation dependencies**
	- To install python if you are in any distribution derived from **Debian** executes the following:
	```bash
    $ sudo apt-get install python2.7
    ```
    - To install easyGUI must have installed the package manager pip, will install python pip and easyGUI follows:
    ```bash
    $ sudo apt-get install python-pip
    $ sudo pip install easygui
    ```
    - To install xterm and wake on lan simply execute the following:
    ```bash
    $ sudo apt-get install wakeonlan xterm
    ```
- **Execution**
	- To execution **WOL GUI** download the repository:
	```bash
    $ git clone https://gitlab.com/carbans/wol-gui.git
    $ cd wol-gui
    ```
    - If we run from the graphical environment simply double click on the file named **lanzar.sh**
    - If we want to launch it from the console we execute the following:
    ```bash
    $ python run.py
    ```
