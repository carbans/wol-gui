# -*- coding: utf-8 -*-
import easygui as eg
import os

#Clase para almacenar la informacion
class AppConfig(eg.EgStore):
 def __init__(self, filename):
  self.MAC = ""
  self.filename = filename

car = os.getcwd()      # Obtiene la ruta de la carpeta actual
ArchivoConfig = os.path.join(car,"","config-homecontrol.cfg")
MiConfig = AppConfig(ArchivoConfig)
MiConfig.restore()

if MiConfig.MAC == "":
	texto = eg.enterbox(msg='Introduce la MAC del servidor',
                                title='Home Control: Añadir MAC',
                                default='00:11:22:33:44:55', strip=True,
                                image=None)
	MAC = texto
	MiConfig.MAC = MAC
   	MiConfig.store()
else:
	MAC = MiConfig.MAC

print MAC
respuesta = respuesta = eg.ynbox(msg='Quiere encender Apolo',
                          title='Home Control: Acceso a Servidores',
                          choices=('Si(1)', 'No(0)'),
                          image=None)
if respuesta == 1:
	cmd = "bash wol.sh {0} ".format(MAC)
	os.system(cmd)
else:
	eg.msgbox(msg='No se ha podido ejecutar el script',
               title='Home Control: Acceso a Servidores', ok_button='Continuar',
               image=None)
	
